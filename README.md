# DUCKS REACT Boilerplate

An overview of the DUCKS Pattern for Organizing a React/Redux Project while also addressing problems like Scalability.

***A peek at how projects used to be organized in the past:***

<p align='center'>
<img src='https://cdn-images-1.medium.com/max/1600/1*HM8M2Agd_TBfU4Zm1_lEJA.png' width='600' alt='npm start'>
</p>

